'use strict';

var MiApp = function()
{
	this.pantallaActual = null;
	this.pantallaNueva = null;
	this.idStepActual = null;
}

MiApp.PANTALLA_INICIO = 'setp0';

MiApp.prototype.inicializar = function(objUsuario)
{
	var _this = this;

	MiApp.prototype.actualizarPantalla = function(nuevaPantalla)
	{
		if(this.pantallaActual === null || nuevaPantalla !== this.pantallaActual.getNombre())
		{	
			if(this.pantallaActual !== null)
			{
				this.pantallaActual.ocultar();
				this.pantallaActual.destruir();
			}

			this.pantallaNueva.mostrar();

			this.pantallaActual = this.pantallaNueva;			
		}
	}

	MiApp.prototype.actualizaPaso = function(step)
	{
		this.pantallaNueva = this.pantallas[step];
	}	

	MiApp.prototype.abrePaso = function(step)
	{	
		$($('#btnStep' + (""+step)).parent()).addClass('open');

		_this.actualizaPaso(step);
		console.log('abre paso')
		this.pantallaNueva.init();

		$('#btnStep' + (""+step)).off('click').on('click', function(e){
			$(window).scrollTop(0);
			_this.actualizaPaso(step);
			_this.actualizarPantalla($(e.target).attr('data-target'));
		});
	}

	MiApp.prototype.guardaProgreso = function(datos, sigPaso)
	{
	}

	MiApp.prototype.imprimirResultados = function()
	{
	}

	//** portada -------------------------------------------------------
	MiApp.prototype.Step0 = function()
	{
		Pantalla.prototype.constructor.call( this ); 

		this.init = function()
		{	
			_this.abrePaso('1');

			this.setNombre('step0');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{
		}
	}

	//** paso 1 -------------------------------------------------------
	MiApp.prototype.Step1 = function()
	{
		Pantalla.prototype.constructor.call( this ); 

		this.init = function()
		{
			this.setNombre('step1');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{
			$('#btnSiguiente1').on('click', function(){
				_this.abrePaso('2');
			});
		}
	}

	//** paso 2 -------------------------------------------------------
	MiApp.prototype.Step2 = function()
	{	
		Pantalla.prototype.constructor.call( this );

		this.init = function()
		{
			this.setNombre('step2');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{
			$('#btnSiguiente2').on('click', function(){
				_this.abrePaso('3');
			});
		}
	}

	//** paso 3 -------------------------------------------------------
	MiApp.prototype.Step3 = function()
	{
		Pantalla.prototype.constructor.call( this );

		this.init = function()
		{
			this.setNombre('step3');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{
		}
	}

	//** paso 4 -------------------------------------------------------
	MiApp.prototype.Step4 = function()
	{
		Pantalla.prototype.constructor.call( this ); 

		this.init = function()
		{
			this.setNombre('step4');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{
		}
	}

	//** paso 5 -------------------------------------------------------
	MiApp.prototype.Step5 = function()
	{
		Pantalla.prototype.constructor.call( this ); 

		this.init = function()
		{
			this.setNombre('step5');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{			
		}
	}

	//** paso 6 -------------------------------------------------------
	MiApp.prototype.Step6 = function()
	{
		Pantalla.prototype.constructor.call( this );

		this.init = function()
		{	
			this.setNombre('step6');
			(!this._done) ? this.StepToDo() : this.StepDone();
		}

		this.StepDone = function()
		{
		}

		this.StepToDo = function()
		{
		}
	}

	// **--------------------------------------------
	_this.Step0.prototype = new Pantalla(); 
	_this.Step0.prototype.constructor = Pantalla;
	_this.Step1.prototype = new Pantalla(); 
	_this.Step1.prototype.constructor = Pantalla;
	_this.Step2.prototype = new Pantalla(); 
	_this.Step2.prototype.constructor = Pantalla;
	_this.Step3.prototype = new Pantalla(); 
	_this.Step3.prototype.constructor = Pantalla;
	_this.Step4.prototype = new Pantalla(); 
	_this.Step4.prototype.constructor = Pantalla;
	_this.Step5.prototype = new Pantalla(); 
	_this.Step5.prototype.constructor = Pantalla;
	_this.Step6.prototype = new Pantalla(); 
	_this.Step6.prototype.constructor = Pantalla;

	var step0 = new _this.Step0();
	var step1 = new _this.Step1();
	var step2 = new _this.Step2();
	var step3 = new _this.Step3();
	var step4 = new _this.Step4();
	var step5 = new _this.Step5();
	var step6 = new _this.Step6();
	
	this.pantallas = [step0, step1, step2, step3, step4, step5, step6];

	this.idStepActual = +MiApp.PANTALLA_INICIO.substr(4,4);
	
	for(var i = 0; i <= this.idStepActual; i ++)
	{
		_this.abrePaso(i);
	}
	
	_this.actualizaPaso(this.idStepActual);
	_this.actualizarPantalla(MiApp.PANTALLA_INICIO);
}