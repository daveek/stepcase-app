'use strict';

var ANIMATION_IN = 'slideInUp';
var ANIMATION_OUT = 'fadeOutLeft';

var Pantalla = function()
{
	this._id = '';
	this._done = false;
	this._opened = false;
}

Pantalla.prototype.setDone = function(done)
{
	this._done = done;
}

Pantalla.prototype.setNombre = function(id)
{
	this._id = id;
}

Pantalla.prototype.getNombre = function()
{
	return this._id;
}

Pantalla.prototype.mostrar = function()
{	
	var $element = $('#' + this._id);

	$element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$element.css('display', 'block').removeClass('animated ' + ANIMATION_IN);
	});

	$element.css({'display':'block', 'z-index':'2'}).addClass('animated ' + ANIMATION_IN);

	this.iluminaBtn();
}

Pantalla.prototype.ocultar = function()
{
	var $element = $('#' + this._id);

	$element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$element.css('display', 'none').removeClass('animated ' + ANIMATION_OUT);
	});

	$element.css({'display':'block', 'z-index':'1'}).addClass('animated ' + ANIMATION_OUT);

	this.apagaBtn();
}

Pantalla.prototype.iluminaBtn = function()
{
	$($('#btnStep' + this._id.substr(4,4)).parent()).removeClass('done').addClass('active');
}

Pantalla.prototype.apagaBtn = function()
{
	$($('#btnStep' + this._id.substr(4,4)).parent()).removeClass('active');
}

Pantalla.prototype.destruir = function() {}