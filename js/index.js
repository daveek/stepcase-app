'use strict';

function ObjUsuarioSt (id, nombre, apellidos, clase)
{ 
	this.id = id;
	this.nombre = nombre;
	this.apellidos = apellidos;
	this.clase = clase;
	this.step = 0;
	this.cuestionario = [0,0,0,0];
	this.ejercicio = [{ prediccion : null },{ prediccion : null },{ incomeLevels: null, speculativeBubble: null, other: null },{ prediccion: null },{ elasticity : null, prediccion: null },{ prediccionUsa : null, prediccionUK : null } ]
}

function ObjUsuarioTch (id, nombre, clase)
{
	this.id = id;
	this.nombre = nombre;
	this.clase = clase;
}


if ($) 
	$(document).ready(mi_onload);
else
	alert("Atencion: su navegador no ha cargado convenientemente el modulo de JQuery. Pongase en contacto con HelpOnline y comunique este incidente.");


function mi_onload()
{
	if ("sessionStorage" in window)
	{
		var la_url="include/comun.php";
		$.getJSON(la_url, funcionJSON);
		
	}else
		alert("Atencion: su navegador no esta actualizado convenientemente. Para poder ver este caso, actualice el mismo a la ultima version diponible en el mercado.");
}


function funcionJSON(json)
{
	var miUsuario;

	if (json.rol === "1" || json.rol === "" || json.rol == "demo.rol")
	{
		miUsuario = new ObjUsuarioSt(json.identificador, json.nombre, json.apellido, json.asignatura, json.rol);	
		document.location = "portada.html";
	}
	else
	{
		miUsuario = new ObjUsuarioTch(json.identificador, json.nombre, json.asignatura);
		document.location = "profesor.html";
	}
	
	sessionStorage.clear();	
	sessionStorage.setItem('myUser', JSON.stringify(miUsuario, undefined));
}