window.cancelRequestAnimFrame = ( function() {
	return window.cancelAnimationFrame          ||
	window.webkitCancelRequestAnimationFrame 	||
	window.mozCancelRequestAnimationFrame   	||
	window.oCancelRequestAnimationFrame      	||
	window.msCancelRequestAnimationFrame     	||
	clearTimeout
} )();

window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame       	|| 
	window.webkitRequestAnimationFrame 			|| 
	window.mozRequestAnimationFrame    			|| 
	window.oRequestAnimationFrame      			|| 
	window.msRequestAnimationFrame     			|| 
	function( callback ){
		window.setTimeout(callback, 1000 / 60);
	};
})();

//-------------------------------------------------------------------------------
var Debugger = function () { };
Debugger.log = function (message) {
   	try { console.log(message); } 
	catch (exception) { return; }
}

//--------------------------------------------------------------------------------
Array.max = function( array ){
	return Math.max.apply( Math, array );
};

Array.min = function( array ){
	return Math.min.apply( Math, array );
};

//comunicación ajax---------------------------------------------------------------
function llamadaBBDD(miUrl, accionPHP)
{
	//construye paquete JSON para enviar a PHP
	//var paqueteJson = {accion:accionPHP, parametro1:miParametro1, parametro2:miParametro2};
	var paqueteJson = {};
	var accionPHP = (accionPHP || "defecto");
	paqueteJson.accion = accionPHP;
	var cont = arguments.length;
	for(i = 2; i < cont; i ++)
	{
		paqueteJson["parametro" + (i - 1)] = arguments[i];	
	}
	//-----------------------------------------

	try
	{
        var request = $.ajax({ type: "POST",
				        url: miUrl,
						async: true,
						dataType: "json",
				     	data: paqueteJson,
				        beforeSend: function(x) {
											if(x && x.overrideMimeType) {
												x.overrideMimeType("application/json;charset=UTF-8");
											}
										},
				        });
        return request;
    }
    catch(ex)
    {
        alert(ex.description);
    }
}

function loadJSON(url)
{
	var data_file = url;
   	var http_request = new XMLHttpRequest();
   
	try{
	  // Opera 8.0+, Firefox, Chrome, Safari
	  http_request = new XMLHttpRequest();
	}catch (e){
	  // Internet Explorer Browsers
	  try{
	     http_request = new ActiveXObject("Msxml2.XMLHTTP");
	  }catch (e) {
	     try{
	        http_request = new ActiveXObject("Microsoft.XMLHTTP");
	     }catch (e){
	        // Something went wrong
	        alert("Your browser broke!");
	        return false;
	     }
	  }
	}

	http_request.open("GET", data_file, true);
	http_request.send();

	return http_request;
}


//eventos animaciones css---------------------------------------------------------
var pfx = ["webkit", "moz", "MS", "o", ""];

function PrefixedEventAdd(element, type, callback) {
	for (var p = 0; p < pfx.length; p++) {
		if (!pfx[p]) type = type.toLowerCase();
			element.addEventListener(pfx[p]+type, callback, false);
	}
}

function PrefixedEventRemove(element, type, listener) {
	for (var p = 0; p < pfx.length; p++) {
		if (!pfx[p]) type = type.toLowerCase();
			element.removeEventListener(pfx[p]+type, listener);
	}
}

//--------------------------------------------------------------------------------
function formateaNombre(string)
{
	var arrayWords;
	var returnString = "";
	var len;
	arrayWords = string.split(" ");
	len = arrayWords.length;
	
	for(i=0;i < len ;i++)
	{
		if(i != (len-1))
			returnString = returnString+primeraLetra(arrayWords[i])+" ";
		else
			returnString = returnString+primeraLetra(arrayWords[i]);
	}
	
	return returnString;
}

function primeraLetra(string)
{
	return string.substr(0,1).toUpperCase()+string.substr(1,string.length).toLowerCase();
}

function formatearNumero(numero,decimales,estilo)
{
	// numero: flotante como me salga del nardo, Gerardo
	// decimales: num de decimales a ver (de 0 a n, entero)
	// estilo: "normal" u otra cosa. "normal" es español
	var cadena, partes;
	var aux,i,cont,parte_entera;
	var base, resto;
	var signo;
	var separador_miles, separador_decimales;
	
	if ((decimales<0) || (decimales!=parseInt(decimales)) || (numero==0))
		return 0;
	
	// Estilo americano
	if (estilo!="normal")
	{
		separador_miles=",";
		separador_decimales=".";
	}else
	{
		separador_miles=".";
		separador_decimales=",";
	}
	// Decimales
	if(decimales>0)
	{
		aux=parseInt(numero*(Math.pow(10,(decimales+1))));
		aux=aux/10;
		aux=Math.round(aux)
		aux=aux/(Math.pow(10,decimales));
	}
	else
		aux=Math.round(numero);
	if (aux<0)
	{
		cadena=""+aux*-1;
		signo="-";
	}else	
	{
		cadena=""+aux;
		signo="";
	}
	resto="";
	if (cadena.indexOf(".")!=-1)
	{
		partes=cadena.split(".");
		base=partes[0];
		resto=partes[1];
	}
	else
		base=cadena;
	cont=3;
	parte_entera="";
	for(i=(base.length-1);i>=0;i--)
	{
		parte_entera=base.substr(i,1)+parte_entera;
		cont--;
		if (cont==0 && i!=0)
		{
			parte_entera=separador_miles+parte_entera;
			cont=3;	
		}
	}
	if (resto!="")
		return signo+parte_entera+separador_decimales+resto;
	else
		return signo+parte_entera;
}

function autotab()
{
	var idElemento = this.getAttribute("id");
	var numElemento = +idElemento.substr(idElemento.length - 1,1);
	var nameElemento = idElemento.substr(0, idElemento.length - 1);
	var next = document.getElementById(nameElemento + (numElemento+1))
	
	if (this.getAttribute && this.value.length==this.getAttribute("maxlength") && !!next) 
    	next.focus();
}